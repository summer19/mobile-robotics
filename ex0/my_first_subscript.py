#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import math

def exp_cos_function(x):
    return np.exp(x)*np.cos(x)
    
def random_vector_a(mu, std_dev, size):
        mean, sigma, N = mu, std_dev, size
        s = np.random.normal(mean, sigma, N)
        return s

def random_vector_b(low, high, size):
        s = np.random.randint(low, high, size)
        return s
    
if __name__ == "__main__":
    x = np.linspace(-2*np.pi, 2*np.pi, 100)
    y = exp_cos_function(x)
    x1 = np.arange(1, 100001, 1)
    mu, std_dev, size = 5, 2, 100000
    y1 = random_vector_a(mu, std_dev, size)
    mean_a, std_deviation_a = np.mean(y1), np.std(y1)  
    print("mu, std_dev, size for a:", mu, std_dev, size)
    print("mean and standard deviation for a:", mean_a, std_deviation_a)
    low, high, size = 0, 10, 100000
    y2 = random_vector_b(low, high, size)
    mean_b, std_deviation_b = np.mean(y2), np.std(y2)  
    print("low, high, size for b:", low, high, size)
    print("mean and standard deviation for b:", mean_b, std_deviation_b)
    #plt.plot(x1, y1, 'g^', x1, y2, 'bs')
    #plt.plot(x, y, 'g^')
    plt.hist(y2, bins = [1,2,3,4,5,6,7,8,9,10])
    plt.hist(y1, bins = [1,2,3,4,5,6,7,8,9,10])
    plt.title("histogram")
    #plt.ylim([0, 10])
    plt.show()

